package shoppingCart;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Web {
    public static void main(String[] args) {

        HashMap<String, ArrayList<String>> items = new HashMap<String, ArrayList<String>>();

        Scanner scr = new Scanner(System.in);

        System.out.println("Enter 'finish' if you do not want to continue");
        String end="";
        if(scr.hasNext())
            end = scr.nextLine();

        while (!end.equalsIgnoreCase("finish")) {

                System.out.println("Are you a Owner/User : ");
                String s = scr.nextLine();

                if (s.equalsIgnoreCase("owner")) {
                    Owner o = new Owner();
                    o.setItems();
                    items = o.getItems();
                } else if (s.equalsIgnoreCase("user")) {
                    User u = new User();
                    //                    items = u.getAvailableItems();
                    if (items.size() == 0)
                        System.out.println("Sorry !! There are no items");
                    else {
                        System.out.println("Available items under each brand are");
                        System.out.println(items);

                        System.out.println("Please enter your required item as [ex : brand_category_subcategory] : ");

                        while (scr.hasNext()) {
                            String item = scr.nextLine();
                            if (item.equals(".")) break;
                            String[] str = item.split("_");
//                                System.out.println("checking : "+str.);
                            if (items.containsKey(str[0] + "_" + str[1])) {
                                u.addToCart(item);
                            }

                        }
                        HashMap<String, Integer> cartItems = u.getCartItems();
                        if (cartItems.size() != 0)
                            System.out.println("Items in cart are : \n" + cartItems);

                    }

                } else {
                    System.out.println("Only a Owner/User can access the items");
                    break;
                }
        }


    }
}

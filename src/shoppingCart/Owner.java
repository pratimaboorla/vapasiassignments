package shoppingCart;

import java.util.*;

public class Owner {
    private static HashSet<String> brands = new HashSet<String>();

    private  static final Scanner scr = new Scanner(System.in);
    private HashMap<String, ArrayList<String>> brandCategories = new HashMap<String,ArrayList<String>>();
    private  HashMap<String, ArrayList<String>> items = new HashMap<String,ArrayList<String>>();

    public  HashMap<String, ArrayList<String>> getItems() {
        return items;
    }

    public void setItems() {

        HashSet<String> brnds = Owner.addBrands();


        Iterator itr = brnds.iterator();

        while(itr.hasNext()){
            String brand = itr.next().toString();
            ArrayList<String> cats = Owner.addCategories(brand);
            brandCategories.put(brand, cats);

            Iterator subitr = cats.iterator();

            String category = "";

            while(subitr.hasNext()){
                category = subitr.next().toString();
                ArrayList<String> subcats  = Owner.addSubCategories(category);
                items.put(category,subcats);
            }

        }
        System.out.println(items);

    }

    private static HashSet<String> addBrands(){

        System.out.println("Enter brand name");


        while(scr.hasNext()){
            String str = scr.nextLine();
            if(str.equals(".")) break;
            brands.add(str);
        }

        System.out.println("Available Brands are : "+brands);

        return brands;
    }

    private static ArrayList<String> addCategories(String brand){
        ArrayList<String> categories = new ArrayList<String>();
        System.out.println("Enter Categories for brand  "+brand + " : ");
        while(scr.hasNext())
        {
            String cat = scr.nextLine();
            if(cat.equals(".")) break;
            categories.add(brand+"_"+cat);

        }
        System.out.println("Available Categories are :" + categories);
        return categories;
    }

    private static ArrayList<String> addSubCategories(String category){
        ArrayList<String> subcategories = new ArrayList<String>();
        System.out.println("Enter SubCategories for category  "+category+" : ");
        while(scr.hasNext())
        {
            String subcat = scr.nextLine();
            if(subcat.equals(".")) break;
            subcategories.add(subcat);

        }

        return subcategories;
    }

}

package shoppingCart;

import java.util.HashMap;
import java.util.Scanner;

public class User {
    private final Scanner scr = new Scanner(System.in);
    private HashMap<String,Integer> cartItems = new HashMap<String,Integer>();

    public HashMap<String, Integer> getCartItems() {
        return cartItems;
    }


    public void addToCart(String s)
    {
        System.out.println("in addToCart");
        int count=1;

        if(cartItems.containsKey(s)) count++;
        else count=1;

        cartItems.put(s,count);
        System.out.println("Item " + s + "  added to cart ");
    }

}

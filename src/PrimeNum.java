import java.util.Scanner;

public class PrimeNum {
    public static void main(String[] args) {
        Scanner scr = new Scanner(System.in);

        System.out.println("Enter the number : ");
        int num = scr.nextInt();
        boolean flag= true;

        for(int i=2;i<=num/2;i++)
        {
            if(num %i == 0) flag=false;
        }
        if(flag) System.out.println(num+" is a Prime number");
        else System.out.println(num+" is not a Prime number");

    }
}

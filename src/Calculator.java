import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {
        Scanner scr = new Scanner(System.in);

        System.out.println("Enter number1: ");
        double num1 = scr.nextDouble();
        System.out.println("Enter number2: ");
        double num2 = scr.nextDouble();
        double result;

        System.out.println("Enter operator ( + , - , * , / , % ))");
        char operator = scr.next().charAt(0);

        switch (operator){
            case '+' : result = num1 + num2;
                        break;
            case '-' : result = num1 - num2;
                        break;
            case '*' : result = num1 * num2;
                        break;
            case '/' : result = num1 / num2;
                        break;
            case  '%' : result = num1 % num2;
                        break;
            default:
                System.out.println("Entered operator is not valid");
                return;
        }

        System.out.println(""+num1 +" "+ operator +" "+ num2 +" = "+result);
    }
}

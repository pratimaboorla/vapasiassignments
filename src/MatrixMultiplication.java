import java.util.Scanner;

public class MatrixMultiplication {
    public static void main(String[] args) {
        Scanner scr= new Scanner(System.in);

        System.out.println("Enter number of rows for first matrix :");
        int mat1rows = scr.nextInt();

        System.out.println("Enter number of columns in first matrix / rows in second matrix :");
        int mat1colsmat2rows = scr.nextInt();

        System.out.println("Enter number of columns in second matrix");
        int mat2cols = scr.nextInt();

        int[][] mat1 = new int[mat1rows][mat1colsmat2rows];
        int[][] mat2 = new int[mat1colsmat2rows][mat2cols];
        int[][] productMatrix = new int[mat1rows][mat2cols];

        System.out.println("Enter elements in first matrix : ");
        for (int i=0;i<mat1rows;i++){
            for (int j =0; j<mat1colsmat2rows;j++) {
                mat1[i][j] = scr.nextInt();
            }
        }

        System.out.println("Enter elements in second matrix : ");
        for (int i=0;i<mat1colsmat2rows;i++){
            for (int j =0; j<mat2cols;j++) {
                mat2[i][j] = scr.nextInt();
            }
        }

        for (int i= 0 ;i<mat1rows;i++){
            for (int j=0 ;j<mat2cols; j++){
             for (int k=0;k<mat1colsmat2rows; k++){
                 productMatrix[i][j] = productMatrix[i][j] + mat1[i][k] * mat2[k][j];
             }
            }
        }

        System.out.println("First matrix is :");
        for (int i=0;i<mat1rows;i++) {
            for (int j = 0; j < mat1colsmat2rows; j++) {
                System.out.print(mat1[i][j]+ " ");
            }
            System.out.println();
        }

        System.out.println("Second matrix is :");
        for (int i=0;i<mat1colsmat2rows;i++) {
            for (int j = 0; j < mat2cols; j++) {
                System.out.print(mat2[i][j]+" ");
            }
            System.out.println();
        }

        System.out.println("Resultant product matrix is :");
        for (int i=0;i<mat1rows;i++) {
            for (int j = 0; j < mat2cols; j++) {
                System.out.print(productMatrix[i][j]+ " ");
            }
            System.out.println();
        }

    }
}

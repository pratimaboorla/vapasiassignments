import java.util.Scanner;

public class MatrixAddition {
    public static void main(String[] args) {
        Scanner scr = new Scanner(System.in);

        System.out.println("Enter number of rows in matrix");
        int rows = scr.nextInt();
        System.out.println("Enter number of columns in matrix");
        int columns = scr.nextInt();

        int[][] matrix1 = new int[rows][columns];
        int[][] matrix2 = new int[rows][columns];
        int[][] resultMatrix = new int[rows][columns];

        System.out.println("Enter the elements in first matrix : ");
        for(int i=0;i<rows;i++) {
            for (int j = 0; j < columns; j++) {
                matrix1[i][j] = scr.nextInt();
            }
        }

        System.out.println("Enter the elements in second matrix: ");
        for(int i=0;i<rows;i++) {
            for (int j = 0; j < columns; j++) {
                matrix2[i][j] = scr.nextInt();
            }
        }

        System.out.println("First matrix is : ");
        for(int i=0;i<rows;i++) {
            for (int j = 0; j < columns; j++) {
                System.out.print(matrix1[i][j]+" ");
            }
            System.out.println();
        }


        System.out.println("Second matrix is : ");
        for(int i=0;i<rows;i++) {
            for (int j = 0; j < columns; j++) {
                System.out.print(matrix2[i][j]+" ");
            }
            System.out.println();
        }


        for (int i=0; i<rows;i++){
            for (int j=0;j<columns;j++){
                resultMatrix[i][j] = matrix1[i][j]+matrix2[i][j];
            }
        }

        System.out.println("Resultant  matrix is : ");
        for(int i=0;i<rows;i++) {
            for (int j = 0; j < columns; j++) {
                System.out.print(resultMatrix[i][j]+" ");
            }
            System.out.println();
        }

    }
}

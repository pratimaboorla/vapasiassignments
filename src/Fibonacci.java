import java.util.Scanner;

public class Fibonacci {
    public static void main(String[] args) {
        Scanner scr = new Scanner(System.in);
        System.out.println("Enter the range of fibonacci series");
        int i=0, j=1;

        int range = scr.nextInt();
        System.out.print(i+"\t"+j+"\t");

        for(int k=0; k<range-2;k++)
        {
            int num = i+j;
            System.out.print(num+"\t");
            i=j;
            j=num;
        }
    }
}

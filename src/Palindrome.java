import java.util.Scanner;

public class Palindrome {
    public static void main(String[] args) {
        Scanner scr = new Scanner(System.in);

        System.out.println("Enter the number :");
        int num= scr.nextInt();

        int temp = num , revNum = 0, remainder;


        while(num!=0){
            remainder = num%10;
            revNum = revNum * 10 + remainder;
            num=num/10;
        }
        if(temp==revNum) System.out.println("Given number is Palindrome");
        else System.out.println("Not a Palindrome number");
    }
}

import java.util.Scanner;

public class Factorial {
    public static void main(String[] args) {
        Scanner scr = new Scanner(System.in);

        System.out.println("Enter number to find factorial");
        int num = scr.nextInt();
        int tot=1;

        for(int i=1; i<=num;i++)
        {
            tot *= i;
        }
        System.out.println("Factorial is : "+tot);
    }
}
